Instalace

Instalace se zahají spuštěním skriptu install.bat. Tento skript zjistí jestli je na stroji nainstalován Python. Pokud není spustí instalátor. Dále se nainstalují VS vývojové nástroje pro C++. Poté se spustí skript install_dependecies.bat, který instaluje Python moduly, které aplikace používá.

Spuštění

Pomocí skriptu start_app.bat.

Upozornění
Při operaci porovnávání modelů může aplikace na chvíli zamrznout. To se děje kvůli delší porovnávací době pro některé modely.
Aplikace se sice tváří jako neaktivní ale na pozadí porovnává modely.
