@ECHO OFF
reg query "hkcu\software\Python"
if ERRORLEVEL 1 GOTO NOPYTHON
ECHO Python was found
GOTO :PYTHONINSTALLED

:NOPYTHON
REM No Python was found
ECHO No python found
ECHO Starting python3 instalation.
executables\python-3.8.3-amd64.exe InstallAllUsers=0 PrependPath=1  Include_launcher=0 Include_test=0 SimpleInstall=1 SimpleInstallDescription="Installing to: C:\Users\%username%\AppData\Local\Programs\Python"
ECHO Finished instalation.

PAUSE

:PYTHONINSTALLED
ECHO Installing buildtools

powershell $ErrorActionPreference = 'Stop'; $VerbosePreference = 'Continue'; $p = Start-Process -Wait -PassThru -FilePath executables\vs_buildtools.exe -ArgumentList '--add Microsoft.VisualStudio.Workload.VCTools --includeOptional --includeRecommended --nocache --wait'

ECHO Finished buildtools instalation

pause

install_dependencies.bat
rem Microsoft.VisualStudio.Workload.NativeDesktop
rem Microsoft.VisualStudio.Workload.VCTools
rem Microsoft.Component.MSBuild Microsoft.VisualStudio.Component.IntelliCode Microsoft.VisualStudio.Component.Roslyn.Compiler Microsoft.VisualStudio.Component.TextTemplating