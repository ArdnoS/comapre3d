@ECHO OFF

:PYTHONINSTALLED

ECHO Installing dependencies
python -m pip install PyQt5
python -m pip install PyOpenGL
python -m pip install PyQt3D
python -m pip install pyperclip
python -m pip install psutil
python -m pip install setuptools
python -m pip install pickle-mixin
python -m pip install pathlib
  
ECHO Finished dependencies instalation
ECHO Would you like to start Compare3D application?
PAUSE

CLS
ECHO 1.Yes
ECHO 2.No
ECHO.

CHOICE /C 12 /M "Enter your choice:"

IF ERRORLEVEL 2 GOTO NO
IF ERRORLEVEL 1 GOTO YES

:YES
ECHO YES put start command here
start_app.bat
GOTO End

:NO
ECHO NO got to eof
GOTO End

:End