import os
import pickle

from PyQt5 import QtCore

import qt
from qt import open_in_program

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QIcon
from PyQt5.QtWidgets import QLabel, QDialog, QFormLayout, QListView, QDialogButtonBox, QFileDialog, QComboBox,\
    QVBoxLayout
from pathlib import *

icon_path = "icons/"


class SelectFileDialog(QDialog):
    def __init__(self, title, message, items, parent=None):
        super(SelectFileDialog, self).__init__(parent=parent)
        self.setWindowIcon(QIcon(str(Path(icon_path + "main_window_icon.png"))))
        self.setFixedWidth(550)
        form = QFormLayout(self)
        form.addRow(QLabel(message))
        self.listView = QListView(self)
        form.addRow(self.listView)
        model = QStandardItemModel(self.listView)
        self.setWindowTitle(title)
        for item in items:
            # create an item with a caption
            standardItem = QStandardItem(item)
            standardItem.setCheckable(True)
            model.appendRow(standardItem)
        self.listView.setModel(model)

        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel, Qt.Horizontal, self)
        form.addRow(buttonBox)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

    def itemsSelected(self):
        selected = []
        model = self.listView.model()
        i = 0
        while model.item(i):
            if model.item(i).checkState():
                selected.append(model.item(i).text())
            i += 1
        # if len(selected) > 1:
        #     return -1
        # else:
        return selected

    def accept(self):
        a = len(self.itemsSelected())
        if a > 1:
            return
        super().accept()


class SelectProgramDialog(QDialog):
    def __init__(self, parent=None):
        super(SelectProgramDialog, self).__init__(parent=parent)
        self.setWindowIcon(QIcon(str(Path(icon_path + "main_window_icon.png"))))
        self.mapToGlobal(QtCore.QPoint(0, 0))
        self.setFixedWidth(550)
        form = QFormLayout(self)

        self.setWindowTitle(" ")
        label = QLabel("No application is associated with this filetype\nWould you like to select it?")
        buttonBox = QDialogButtonBox(QDialogButtonBox.Yes | QDialogButtonBox.Cancel, Qt.Horizontal, self)
        form.addRow(label)
        form.addRow(buttonBox)

        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)


class SelectFilesToConvertDialog(QDialog):
    def __init__(self, title, message, items, parent=None):
        super(SelectFilesToConvertDialog, self).__init__(parent=parent)
        self.setWindowIcon(QIcon(str(Path(icon_path + "main_window_icon.png"))))
        self.setFixedWidth(550)
        form = QVBoxLayout(self)
        form.addWidget(QLabel(message))
        self.listView = QListView(self)
        form.addWidget(self.listView)
        model = QStandardItemModel(self.listView)
        self.setWindowTitle(title)
        for item in items:
            # create an item with a caption
            standardItem = QStandardItem(item)
            standardItem.setCheckable(True)
            model.appendRow(standardItem)

        self.listView.setModel(model)
        self.combo_box = QComboBox()
        self.combo_box.addItem("ply")
        self.combo_box.addItem("obj")
        self.combo_box.addItem("stl")
        self.combo_box.addItem("wrl")
        self.combo_box.addItem("off")
        self.combo_box.addItem("binvox")

        form.addWidget(self.combo_box)
        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel, Qt.Horizontal, self)
        form.addWidget(buttonBox)

        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

    def get_combo_box_value(self):
        return str(self.combo_box.currentText())

    def itemsSelected(self):
        selected = []
        model = self.listView.model()
        i = 0
        while model.item(i):
            if model.item(i).checkState():
                selected.append(model.item(i).text())
            i += 1
        return selected


def show_select_file_dialog(self, models, similar_models):
    items = []
    if type(models) is list:
        for x in range(len(models)):
            items.append(str(Path(models[x])))
    else:
        if models is not None:
            items.append(str(Path(models)))

    if type(similar_models) is list:
        for x in range(len(similar_models)):
            items.append(str(Path(similar_models[x])))
    else:
        if similar_models is not None:
            items.append(str(Path(similar_models)))

    dial = SelectFileDialog("Please select just one file to open", "List of selected files", items, self)
    if dial.exec_() == QDialog.Accepted:
        return dial.itemsSelected()


def show_select_program_dialog(self):
    dial = SelectProgramDialog()
    if dial.exec_() == QDialog.Accepted:
        print("before " + str(self.open_in_program))
        self.open_in_program, filters = QFileDialog.getOpenFileName(self, 'Select program to open your model')
        print("after " + str(self.open_in_program))
        if os.path.isfile(str(self.open_in_program)):
            pickle.dump(self.open_in_program, open(Path(os.path.expanduser("~")
                                                        + "/Documents/Compare3D/open_in_prog.ini"), "wb"))
        return True
    else:
        return False


def show_convert_files_dialog(self, models, similar_models):
    items = []
    if type(models) is list:
        for x in range(len(models)):
            items.append(str(Path(models[x])))
    else:
        if models is not None:
            items.append(str(Path(models)))

    if type(similar_models) is list:
        for x in range(len(similar_models)):
            items.append(str(Path(similar_models[x])))
    else:
        if similar_models is not None:
            items.append(str(Path(similar_models)))
    dial = SelectFilesToConvertDialog("Which files you want to convert?", "List of selected files", items, self)
    if dial.exec_() == QDialog.Accepted:
        print("accepted")
        return dial.itemsSelected(), dial.get_combo_box_value()
    else:
        # Dialog was canceled -> return empty array
        return [], "-1"
