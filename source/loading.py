import os
from os import listdir
from os.path import isfile, join
import ntpath
import reeb
import os.path

from PyQt5 import QtCore
from PyQt5.QtWidgets import QProgressBar
from pathlib import *
import pickle


class Window(QProgressBar):
    """
    Main window progress bar

    ...

    Attributes
    ----------
    main_window
        instance of main_window

    models_dir : basestring

    descriptors_dir : basestring

    java_path : basestring

    Methods
    -------
    startProgressBar
        start thread with progressbar
    setProgressVal
        update number of extracted models
    killthread
        stop extraction

    """

    def __init__(self, main_window, models_dir, descriptors_dir, java_path):
        super().__init__()

        self.models_dir = models_dir
        self.descriptors_dir = descriptors_dir
        self.java_path = java_path
        self.main_window = main_window

        self.setMinimumWidth(30)
        self.setMaximumHeight(50)

        # get list of models in models directory
        all_files = reeb.getListOfFiles(self.models_dir)
        self.wrl_files = []
        for x in range(len(all_files)):
            tmp = os.path.splitext(all_files[x])[1]
            if tmp == ".wrl":
                self.wrl_files.append(all_files[x])

        # get size of models
        wrl_models_plus_size = []
        for i in range(len(self.wrl_files)):
            size = Path(self.wrl_files[i]).stat().st_size
            wrl_models_plus_size.append(str(self.wrl_files[i]) + "|" + str(size))

        models_to_extract = []

        EOF_FileNotFound = False
        try:
            wrl_files_info_ini = pickle.load(open(Path(os.path.expanduser("~")
                                              + "/Documents/Compare3D/wrl_files_info.ini"), "rb"))
        except (EOFError, FileNotFoundError):
            wrl_files_info_ini = []
            EOF_FileNotFound = True
            print("EOF or file doesn't exist")
        except:
            wrl_files_info_ini = []
            # if load from ini isn't possible extract all files
            print("Can't load wrl_files_info.ini")

        # check which files were changed
        if wrl_files_info_ini:
            # iterate all .wrl files in self.models_dir
            for i in range(len(wrl_models_plus_size)):
                # split[0] = filepath+name | split[1]=filesize
                split = wrl_models_plus_size[i].split("|")
                if any(split[0] in s for s in wrl_files_info_ini):
                    for j in range(len(wrl_files_info_ini)):
                        # wrl_files_info_ini: tmp[0] = filepath+name | tmp[1]=filesize
                        tmp = wrl_files_info_ini[j].split("|")
                        # check if file was changed
                        if split[0] == tmp[0]:
                            # size is different -> it was changed
                            if split[1] != tmp[1]:
                                models_to_extract.append(split[0])
                                continue
                            # It wasn't changed -> continue
                            else:
                                continue
                else:
                    models_to_extract.append(split[0])
        printArray(models_to_extract, "models_to_extract")
        # get count of models to extract
        if models_to_extract:
            file_count = len(models_to_extract)
        else:  # if no files to extract check if some descriptors were deleted
            descriptors = [f for f in listdir(self.descriptors_dir) if isfile(join(self.descriptors_dir, f))]
            file_count = len(self.wrl_files)

            tmp_desc = []
            for i in range(len(wrl_files_info_ini)):
                split = wrl_files_info_ini[i].split("|")
                tmp = ntpath.basename(split[0])
                filename, extension = os.path.splitext(tmp)
                if extension == ".wrl":
                    filename = filename + ".mrg"

                tmp_desc.append(filename)

            for i in range(len(tmp_desc)):
                if any(tmp_desc[i] in s for s in descriptors):
                    continue
                else:
                    for j in range(len(self.wrl_files)):
                        d, e = os.path.splitext(tmp_desc[i])
                        if self.wrl_files[j].find(d) != -1:
                            # if found descriptor was deleted for valid model extract it again
                            models_to_extract.append(self.wrl_files[j])
                        else:
                            continue
            if EOF_FileNotFound:
                models_to_extract = self.wrl_files

        if models_to_extract:
            file_count = len(models_to_extract)

        descriptors = [f for f in listdir(self.descriptors_dir) if isfile(join(self.descriptors_dir, f))]

        list_of_models = getListOfFiles(self.models_dir)
        if str(list_of_models) == "-1":
            self.main_window.pop_message_box("Please select models directory")
            return

        # Get list of models with .mrg extension
        for i in range(len(list_of_models)):
            base_name = ntpath.basename(list_of_models[i])
            filename, extension = os.path.splitext(base_name)
            if extension == ".wrl":
                extension = ".mrg"
                list_of_models[i] = filename + extension

        # delete descriptors of files that were deleted
        for i in range(len(descriptors)):
            if any(descriptors[i] in s for s in list_of_models):
                continue
            else:
                filename, extension = os.path.splitext(descriptors[i])
                if extension == ".mrg":
                    print("to delete: " + self.descriptors_dir + "\\" + descriptors[i])
                    os.remove(self.descriptors_dir + "\\" + descriptors[i])

        self.setMaximum(file_count)
        self.setStyleSheet("QProgressBar {border: 2px solid grey;border-radius:8px;padding:1px}"
                           "QProgressBar::chunk {background:yellow}")
        self.setAlignment(QtCore.Qt.AlignCenter)

        # extract only new and changed files
        self.thread = reeb.MyThread(main_window, self.models_dir, self.descriptors_dir, self.java_path, self.wrl_files,
                                    models_to_extract, wrl_files_info_ini)
        self.startProgressBar(main_window)
        self.show()

    def startProgressBar(self, main_window):
        self.thread.change_value.connect(self.setProgressVal)
        main_window.extract_descriptors_button.setEnabled(False)
        self.thread.setTerminationEnabled(True)
        self.thread.start()

    def setProgressVal(self, val):
        self.setValue(val)

    # stop extraction
    def killthread(self):
        self.thread.stop()


def printArray(array, name):
    if array:
        for i in range(len(array)):
            print(str(name) + "[" + str(i) + "]: " + str(array[i]))

        print(str(name) + "length: " + str(len(array)))


# returns list of files in directory and it's subdirectories
def getListOfFiles(dirName):
    # create a list of file and sub directories
    # names in the given directory
    try:
        listOfFile = os.listdir(dirName)
    except OSError as ex:
        return "-1"
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(str(Path(fullPath)))

    return allFiles
