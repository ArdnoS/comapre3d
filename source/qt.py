import os.path
import shutil
import sys
import subprocess
import view_model
import pickle
import reeb
import dialog
import platform
import pyperclip

from os import path
from pathlib import *
from PyQt5 import QtCore, QtWidgets
from PyQt5.Qt3DCore import QEntity, QTransform
from PyQt5.Qt3DInput import *
from PyQt5.QtCore import *
from PyQt5.QtGui import QVector3D, QQuaternion, QIcon, QWindow, QStandardItemModel
from PyQt5.Qt3DRender import *
from PyQt5.Qt3DExtras import *
from PyQt5.QtWidgets import QWidget, QGridLayout, QMainWindow, QCheckBox, QPushButton, QComboBox, QLabel, QAction, \
    QToolBar, QStatusBar, QFileDialog, QApplication, QVBoxLayout, QHBoxLayout, QFileSystemModel, \
    QTreeView, QLineEdit, QAbstractItemView, QSizePolicy, QInputDialog, QGroupBox, QDialog, QProgressBar, QMessageBox

icon_path = "icons/"
startpath = os.getcwd()

global models_directory_path
global descriptors_directory_path
global last_model
axis_x = 1.0
axis_y = 0.0
axis_z = 0.0
rotation_angle = 25.0

threshold = 70
open_in_program = "_"


def printArray(array, name):
    if array:
        for i in range(len(array)):
            print(str(name) + "[" + str(i) + "]: " + str(array[i]))

        print(str(name) + "length: " + str(len(array)))


class MainWindow(QMainWindow):
    """
        Main window of application

        ...

        Methods
        -------
        similar_model_add_row

        init_similar_model_view
            Initialization of similar models view

        similar_model_context_menu
            Ads optins to context menu

        init_filesystem_treeview
            Diplays filesystem from current folder

        tree_view_doubleclicked
            Changes root_path and displays treeview with selected root_path

        get_path_of_selected_items
              Returns path of selected items or path of firs selected item

        similar_models_get_data
            Returns model name or model path or model similarity

        up_button_clicked
            Changes root path for filesystem view

        cancel_button_clicked
            stop's descriptors extraction

        compare_models_action_handle
            Compare selected model with every other in models directory

        view_model_action_handle
            Display selected model inside main window

        get_models_directory
            return path to user selected folder with models

        get_descriptors_directory
            return path to user selected folder with descriptors

        select_models_dir_action_handle
            Select and save path to folder with models

        select_decriptors_dir_action_handle
            Select and save path to folder with descriptors

        set_threshold_action_handle
            set's and save's threshold

        set_open_in_program_action_handle
            set's and save's exe to open models

        set_java_path_action_handle
            set's and save's path to java

        open_model_in_default_program_action_handle
            open file with windows defaul program or with user selected program

        open file with windows defaul program or with user selected program
            open file with windows defaul program or with user selected program

        open_file_destination_action_handle
            open windows explorer on se;ected file destination

        load_project_structure
            set rootpath for view of filesystem

        set_main_window_start_position
            set starting position for main window

        similar_model_context_menu_open_in_prog
            open similar model via context menu of similar model view

        similar_model_context_menu_open_in_folder
            open windows explorer with file selected via similar model view context menu

        pop_message_box
            show message box with custom message
    """
    # Values for similar_model_view
    FROM, SUBJECT, DATE = range(3)

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.open_in_program = open_in_program
        # Compile reeb utility
        # reeb.compile_reeb_graph()

        try:
            self.java_path = pickle.load(open(Path(os.path.expanduser("~")
                                                   + "/Documents/Compare3D/java_path.ini"), "rb"))
        except:
            self.java_path, filters = QFileDialog.getOpenFileName(self, "Select path to java.exe")
            pickle.dump(self.java_path, open(Path(os.path.expanduser("~")
                                                  + "/Documents/Compare3D/java_path.ini"), "wb"))

        # Main wondow icon
        self.setWindowIcon(QIcon(str(Path(icon_path + "main_window_icon.png"))))

        # Change root dir to upper dir button
        self.up_button = QPushButton()
        self.up_button.setIcon(QIcon(str(Path(icon_path + "arrow-090.png"))))
        self.up_button.setToolTip("Upper directory")
        self.up_button.setMaximumSize(35, 25)
        self.up_button.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        self.up_button.clicked.connect(self.up_button_clicked)

        self.cancel_button = QPushButton()
        self.cancel_button.setIcon(QIcon(str(Path(icon_path + "cancel_icon.png"))))
        self.cancel_button.setToolTip("Cancel extraction")
        self.cancel_button.setMaximumSize(35, 25)
        self.cancel_button.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        self.cancel_button.clicked.connect(self.cancel_button_clicked)

        self.set_main_window_start_position(30, 50, 1024, 640)
        self.setWindowTitle("Compare 3D")

        # Init information for treeview, set root path for filesystem
        try:  # Load user saved root path location if it exist
            self.root_path = pickle.load(open(Path(os.path.expanduser("~")
                                                   + "/Documents/Compare3D/root_path.ini"), "rb"))
        except:
            print("Can't load root_path.ini")
            self.root_path = startpath

        self.previous_root_path = None

        self.filesystem_model = QFileSystemModel(self)
        self.filesystem_model.setRootPath(self.root_path)
        self.filesystem_model.setReadOnly(False)

        self.index_root = self.filesystem_model.index(self.filesystem_model.rootPath())

        # Init group box
        self.similar_model_group_box = QGroupBox("Similar models")

        # Init context menu
        self.tree_view_context_menu = QtWidgets.QMenu()
        self.tree_view_context_menu.addAction("Delete", self.tree_view_context_menu_delete)
        self.tree_view_context_menu.addAction("Copy", self.tree_view_context_menu_copy)
        self.tree_view_context_menu.addAction("Paste", self.tree_view_context_menu_paste)
        self.tree_view_context_menu.addAction("Create directory", self.tree_view_context_menu_mkdir)

        # Init treeview
        self.tree_view = QTreeView(self)
        self.tree_view.doubleClicked.connect(self.tree_view_doubleclicked)
        self.init_filesystem_treeview()

        self.similar_model_view = QTreeView()
        self.similar_model_layout = QHBoxLayout()
        self.similar_model_group_box.setLayout(self.similar_model_layout)
        self.similar_model_group_box.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        self.similar_model_group_box.setMaximumWidth(400)
        self.similar_models_model = QStandardItemModel(0, 3, self)
        self.init_similar_model_view()

        # Add empty progress bar
        self.dummy_progressbar = QProgressBar()
        self.dummy_progressbar.setMinimumWidth(10)
        self.dummy_progressbar.setMaximumHeight(20)
        self.dummy_progressbar.setStyleSheet("QProgressBar {border: 2px solid grey;border-radius:8px;padding:1px}"
                                             "QProgressBar::chunk {background:yellow}")
        self.dummy_progressbar.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)

        # Init layouts
        self.layout_display_model = QVBoxLayout()
        self.layout_1 = QVBoxLayout()
        self.layout_2 = QHBoxLayout()
        self.layout_3 = QVBoxLayout()

        self.layout_1.addLayout(self.layout_2)
        self.layout_1.addLayout(self.layout_3)

        self.top_layout = QGridLayout()
        self.top_layout.addLayout(self.layout_1, 0, 0)
        self.top_layout.addLayout(self.layout_display_model, 0, 1)

        self.layout_2.addWidget(self.up_button)
        self.layout_2.addWidget(self.cancel_button)
        self.layout_2.addWidget(self.dummy_progressbar)

        self.layout_3.addWidget(self.tree_view)

        # Display 3D model in layout as widget
        self.view = Qt3DWindow()
        self.scene = None
        # Camera.
        self.camera = self.view.camera()
        # Root entity.
        self.rootEntity = QEntity()
        # Material.
        self.material = QPhongMaterial(self.rootEntity)
        # Torus.
        self.torusEntity = QEntity(self.rootEntity)
        self.torusMesh = QMesh()
        self.torusTransform = QTransform()

        # Add container to layout
        self.container = self.createWindowContainer(self.view)
        self.container.setMinimumWidth(350)
        self.container.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)

        self.layout_display_model.addWidget(self.container)

        # Display last viewed model if any exist
        try:
            if os.path.exists(last_model):
                view_model.view_model(self, last_model)
        except:
            print("Cant load last model")
            pass

        # Init central widget
        self.widget = QWidget()
        self.widget.setLayout(self.top_layout)
        self.setCentralWidget(self.widget)

        # Menu
        self.bar = self.menuBar()
        menu_bar = self.bar.addMenu("Set")

        # Menu options
        self.select_models_dir_action = QAction("Set models dirirectory", self)
        self.select_models_dir_action.triggered.connect(self.select_models_dir_action_handle)
        menu_bar.addAction(self.select_models_dir_action)

        self.select_descriptors_dir_action = QAction("Set descriptors directory ", self)
        self.select_descriptors_dir_action.triggered.connect(self.select_decriptors_dir_action_handle)
        menu_bar.addAction(self.select_descriptors_dir_action)

        self.set_threshold_action = QAction("Set threshold", self)
        self.set_threshold_action.triggered.connect(self.set_threshold_action_handle)
        menu_bar.addAction(self.set_threshold_action)

        self.set_open_in_program = QAction("Set 3rd party program to open models", self)
        self.set_open_in_program.triggered.connect(self.set_open_in_program_action_handle)
        menu_bar.addAction(self.set_open_in_program)

        self.unset_open_in_program = QAction("Unset 3rd party program to open models", self)
        self.unset_open_in_program.triggered.connect(self.unset_open_in_program_action_handle)
        menu_bar.addAction(self.unset_open_in_program)

        self.set_java_path = QAction("Set java path", self)
        self.set_java_path.triggered.connect(self.set_java_path_action_handle)
        menu_bar.addAction(self.set_java_path)

        self.set_root_path = QAction("Set root path", self)
        self.set_root_path.triggered.connect(self.set_root_path_action_handle)
        menu_bar.addAction(self.set_root_path)

        # Toolbars
        self.main_toolbar = QToolBar("My main_toolbar")
        self.main_toolbar.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.addToolBar(self.main_toolbar)

        # Toolbar Buttons
        self.extract_descriptors_button = QAction(QIcon(str(Path(icon_path + "extract.png"))), "Extract descriptors",
                                                  self)
        self.extract_descriptors_button.setStatusTip("Extract descriptors from selected folder")
        self.extract_descriptors_button.triggered.connect(lambda y: reeb.extract_descriptors(
            self, models_directory_path, descriptors_directory_path))
        self.extract_descriptors_button.setCheckable(False)
        self.main_toolbar.addAction(self.extract_descriptors_button)

        self.view_model_action = QAction(QIcon(str(Path(icon_path + "eye.png"))), "View model", self)
        self.view_model_action.setStatusTip("View model")
        self.view_model_action.triggered.connect(self.view_model_action_handle)
        self.view_model_action.setCheckable(False)
        self.main_toolbar.addAction(self.view_model_action)
        self.main_toolbar.addSeparator()

        self.convert_model_action = QAction(QIcon(str(Path(icon_path + "convert_model_icon.png"))),
                                            "Convert model", self)
        self.convert_model_action.setStatusTip("Convert model")
        self.convert_model_action.triggered.connect(self.convert_model_action_handle)
        self.convert_model_action.setCheckable(False)
        self.main_toolbar.addAction(self.convert_model_action)
        self.main_toolbar.addSeparator()

        self.compare_models_action = QAction(QIcon(str(Path(icon_path + "compare_icon.png"))), "Compare models", self)
        self.compare_models_action.setStatusTip("Comapre two selected models")
        self.compare_models_action.triggered.connect(lambda y: self.compare_models_action_handle(self))
        self.compare_models_action.setCheckable(False)
        self.main_toolbar.addAction(self.compare_models_action)
        self.main_toolbar.addSeparator()

        self.open_model_in_default_program_action = QAction(QIcon(str(Path(icon_path + "open_in_popup.png"))),
                                                            "Open model in selected or windows default program", self)
        self.open_model_in_default_program_action.setStatusTip("Open model in selected program")
        self.open_model_in_default_program_action.triggered.connect(
            lambda y: self.open_model_in_default_program_action_handle(y))
        self.open_model_in_default_program_action.setCheckable(False)
        self.main_toolbar.addAction(self.open_model_in_default_program_action)
        self.main_toolbar.addSeparator()

        self.open_file_destination = QAction(QIcon(str(Path(icon_path + "open_file_folder.png"))),
                                             "Open file destination", self)
        self.open_file_destination.setStatusTip("Open file destination")
        self.open_file_destination.triggered.connect(lambda y: self.open_file_destination_action_handle(y))
        self.open_file_destination.setCheckable(False)
        self.main_toolbar.addAction(self.open_file_destination)
        self.main_toolbar.addSeparator()

        # Model rotation down
        self.rotate_model_down_action = QAction(QIcon(str(Path(icon_path + "rotate_down.png"))), "Rotate model down",
                                                self)
        self.rotate_model_down_action.setStatusTip("Rotate model down")
        self.rotate_model_down_action.triggered.connect(lambda y: view_model.rotate_model_down_action_handle(self, y))
        self.rotate_model_down_action.setCheckable(False)
        self.main_toolbar.addAction(self.rotate_model_down_action)
        self.main_toolbar.addSeparator()
        # Model rotation up
        self.rotate_model_up_action = QAction(QIcon(str(Path(icon_path + "rotate_up.png"))), "Rotate model up", self)
        self.rotate_model_up_action.setStatusTip("Rotate model up")
        self.rotate_model_up_action.triggered.connect(lambda y: view_model.rotate_model_up_action_handle(self, y))
        self.rotate_model_up_action.setCheckable(False)
        self.main_toolbar.addAction(self.rotate_model_up_action)
        self.main_toolbar.addSeparator()
        # Model rotation left
        self.rotate_model_left_action = QAction(QIcon(str(Path(icon_path + "rotate_left.png"))), "Rotate model left",
                                                self)
        self.rotate_model_left_action.setStatusTip("Rotate model left")
        self.rotate_model_left_action.triggered.connect(lambda y: view_model.rotate_model_left_action_handle(self, y))
        self.rotate_model_left_action.setCheckable(False)
        self.main_toolbar.addAction(self.rotate_model_left_action)
        self.main_toolbar.addSeparator()
        # Model rotation right
        self.rotate_model_right_action = QAction(QIcon(str(Path(icon_path + "rotate_right.png"))), "Rotate model right",
                                                 self)
        self.rotate_model_right_action.setStatusTip("Rotate model right")
        self.rotate_model_right_action.triggered.connect(lambda y: view_model.rotate_model_right_action_handle(self, y))
        self.rotate_model_right_action.setCheckable(False)
        self.main_toolbar.addAction(self.rotate_model_right_action)
        self.main_toolbar.addSeparator()

        # Status bar
        self.status_bar = QStatusBar(self)
        self.setStatusBar(self.status_bar)

        # Add widgets to toolbar
        self.main_toolbar.addAction(self.view_model_action)
        self.main_toolbar.addSeparator()

        self.show()

    def similar_model_add_row(self, row, row1, row2):
        self.similar_models_model.insertRow(0)
        self.similar_models_model.setData(self.similar_models_model.index(0, self.FROM), row)
        self.similar_models_model.setData(self.similar_models_model.index(0, self.SUBJECT), row1)
        self.similar_models_model.setData(self.similar_models_model.index(0, self.DATE), row2)

    # Initialization of similar models view
    def init_similar_model_view(self):
        self.similar_model_layout.addWidget(self.similar_model_view)
        self.similar_model_view.setModel(self.similar_models_model)
        # set columns names
        self.similar_models_model.setHeaderData(self.FROM, Qt.Horizontal, "Name")
        self.similar_models_model.setHeaderData(self.SUBJECT, Qt.Horizontal, "Path")
        self.similar_models_model.setHeaderData(self.DATE, Qt.Horizontal, "Similarity %")
        # set size for columns
        self.similar_model_view.header().resizeSection(0, 100)
        self.similar_model_view.header().resizeSection(1, 160)
        self.similar_model_view.header().resizeSection(2, 50)
        # context menu
        self.similar_model_view.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.similar_model_view.customContextMenuRequested.connect(self.similar_model_context_menu_handle)
        # widget size policy
        self.similar_model_view.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        self.similar_model_view.selectionModel().selectionChanged.connect(
            lambda x: self.similar_models_get_data(False, True, True, True))
        self.similar_model_view.setSelectionMode(QAbstractItemView.ExtendedSelection)

    # Ads optins to context menu
    def similar_model_context_menu_handle(self, point):
        self.similar_model_context_menu = QtWidgets.QMenu()
        self.similar_model_context_menu.addAction("Open in application",
                                                  lambda z=point: self.similar_model_context_menu_open_in_prog(z))
        self.similar_model_context_menu.addAction("Open model folder",
                                                  lambda z=point: self.similar_model_context_menu_open_in_folder(z))
        self.similar_model_context_menu.exec_(self.similar_model_view.mapToGlobal(point))

    # Diplays filesystem from current folder
    def init_filesystem_treeview(self):
        self.tree_view.setModel(self.filesystem_model)
        self.tree_view.setRootIndex(self.index_root)
        self.tree_view.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
        self.tree_view.setMaximumSize(400, 1000)
        self.tree_view.hideColumn(1)
        self.tree_view.hideColumn(2)
        self.tree_view.hideColumn(3)
        self.tree_view.selectionModel().selectionChanged.connect(lambda x: self.get_path_of_selected_items(True))
        self.tree_view.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.tree_view.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.tree_view.customContextMenuRequested.connect(self.tree_view_context_menu_handle)

    # Context menu on filesystem
    def tree_view_context_menu_handle(self, point):
        self.tree_view_context_menu.exec_(self.tree_view.mapToGlobal(point))

    # Changes root_path and displays treeview with selected root_path
    def tree_view_doubleclicked(self):
        tmp = self.previous_root_path
        self.previous_root_path = self.root_path
        self.root_path = self.get_path_of_selected_items(True)
        if path.isdir(self.root_path):
            self.filesystem_model.setRootPath(self.root_path)
            self.index_root = self.filesystem_model.index(self.filesystem_model.rootPath())
            self.init_filesystem_treeview()
        else:
            self.root_path = self.previous_root_path
            self.previous_root_path = tmp

    # Returns path of selected items or path of firs selected item
    @QtCore.pyqtSlot(QtCore.QModelIndex)
    def get_path_of_selected_items(self, return_one_model):
        try:
            models = []
            selected_indeces = self.tree_view.selectedIndexes()
            y = len(selected_indeces)
            for x in range(y):
                models.append(self.filesystem_model.filePath(selected_indeces[x]))
            if return_one_model:
                return models[0]
            else:
                return models
        except:
            print("Nothing selected")
            pass

    # Returns model name or model path or model similarity
    def similar_models_get_data(self, return_one_model, model_name, model_path, similarity):
        try:
            models = []
            for i in self.similar_model_view.selectedIndexes():
                if model_name:
                    if i.column() == 0:
                        models.append(i.data())
                        if model_name and model_path and similarity:
                            print("similar_models_get_data:slected model name: " + str(models[0]))
                        else:
                            print("similar_models_get_data:slected model name: " + str(models[0]))
                if model_path:
                    if i.column() == 1:
                        models.append(i.data())
                        if model_name and model_path and similarity:
                            print("similar_models_get_data:slected model path: " + str(models[1]))
                            pyperclip.copy(str(Path(models[1])))
                        else:
                            print("similar_models_get_data:slected model path: " + str(models[0]))
                            pyperclip.copy(str(Path(models[0])))

                if similarity:
                    if i.column() == 2:
                        models.append(i.data())
                        if model_name and model_path and similarity:
                            print("slected model similarity: " + str(models[2]))
                        else:
                            print("slected model similarity: " + str(models[0]))
            if return_one_model:
                return models[0]
            else:
                return models
        except:
            print("No similar model selected")

    # changes root path for filesystem view
    def up_button_clicked(self):
        self.previous_root_path = self.root_path
        head, tail = os.path.split(self.root_path)
        self.root_path = head
        self.filesystem_model.setRootPath(self.root_path)
        self.index_root = self.filesystem_model.index(self.filesystem_model.rootPath())
        self.init_filesystem_treeview()

    # stop's descriptors extraction
    def cancel_button_clicked(self):
        try:
            self.progressbar.killthread()
        except:
            print("No extraction to cancel")

    # Compare selected model with every other in models directory
    def compare_models_action_handle(self, y):
        global threshold
        global descriptors_directory_path
        models = self.get_path_of_selected_items(False)
        similar_models = self.similar_models_get_data(False, False, True, False)
        if not models and not similar_models:
            self.pop_message_box("Please select model")
        elif models and not similar_models:
            if len(models) == 1:
                reeb.compare_models(self, models_directory_path, models[0], threshold, descriptors_directory_path)
            else:
                selected = dialog.show_select_file_dialog(self, models, similar_models)
                if selected:
                    reeb.compare_models(self, models_directory_path, selected[0], threshold, descriptors_directory_path)
        elif not models and similar_models:
            if len(similar_models) == 1:
                reeb.compare_models(self, models_directory_path, similar_models[0], threshold,
                                    descriptors_directory_path)
            else:
                selected = dialog.show_select_file_dialog(self, models, similar_models)
                if selected:
                    reeb.compare_models(self, models_directory_path, selected[0], threshold, descriptors_directory_path)
        else:
            selected = dialog.show_select_file_dialog(self, models, similar_models)
            if selected:
                reeb.compare_models(self, models_directory_path, selected[0], threshold, descriptors_directory_path)

    # display selected model inside main window
    def view_model_action_handle(self, s):
        global last_model
        model = self.get_path_of_selected_items(False)
        similar_model_path = self.similar_models_get_data(False, False, True, False)
        if not model and not similar_model_path:
            view_model.view_model(self, last_model)
        elif model and not similar_model_path:
            if len(model) == 1:
                last_model = model[0]
                view_model.view_model(self, model[0])
            else:
                selected = dialog.show_select_file_dialog(self, model, similar_model_path)
                if selected:
                    last_model = selected[0]
                    view_model.view_model(self, selected[0])

        elif not model and similar_model_path:
            if len(similar_model_path) == 1:
                last_model = similar_model_path[0]
                view_model.view_model(self, similar_model_path[0])
            else:
                selected = dialog.show_select_file_dialog(self, model, similar_model_path)
                if selected:
                    last_model = selected[0]
                    view_model.view_model(self, selected[0])
        else:
            selected = dialog.show_select_file_dialog(self, model, similar_model_path)
            if selected:
                view_model.view_model(self, selected[0])

    # Convert models
    def convert_model_action_handle(self):
        models = self.get_path_of_selected_items(False)
        similar_models = self.similar_models_get_data(False, False, True, False)
        selected, file_format = dialog.show_convert_files_dialog(self, models, similar_models)
        # Check if something was selected
        if selected:
            for i in range(len(selected)):
                if file_format == "binvox":
                    subprocess.Popen("executables\\binvox.exe " + selected[i])
                filename, extension = os.path.splitext(selected[i])
                # Check if selected file is model
                if extension == ".wrl" or extension == ".ply" or extension == ".obj" or extension == ".stl" or \
                        extension == ".off":
                    subprocess.Popen("executables\\meshconv.exe " + selected[i] + " " + "-c " + file_format)
        else:  # Nothing selected -> do nothing
            print(" ")

    # return path to user selected folder with models
    def get_models_directory(self):
        folder = str(QFileDialog.getExistingDirectory(self, "Select folder with models"))
        return folder

    # return path to user selected folder with descriptors
    def get_descriptors_directory(self):
        folder = str(QFileDialog.getExistingDirectory(self, "Select folder with descriptors"))
        return folder

    # select and save path to folder with models
    def select_models_dir_action_handle(self):
        global models_directory_path
        try:
            models_directory_path = window.get_models_directory()
            pickle.dump(models_directory_path, open(Path(os.path.expanduser("~")
                                                         + "/Documents/Compare3D/models_path.ini"), "wb"))
        except:
            print("Can't write to models_path.ini file")

    # select and save path to folder with descriptors
    def select_decriptors_dir_action_handle(self):
        global descriptors_directory_path
        try:
            descriptors_directory_path = window.get_descriptors_directory()
            pickle.dump(descriptors_directory_path, open(Path(os.path.expanduser("~")
                                                              + "/Documents/Compare3D/descriptors_path.ini"), "wb"))
        except:
            print("Can't write to descriptors_path.ini file")

    # Set new root path for filesystem and save it for further use
    def set_root_path_action_handle(self):
        folder = str(QFileDialog.getExistingDirectory(self, "Select new root path"))
        self.root_path = str(Path(folder))
        self.filesystem_model.setRootPath(self.root_path)
        self.index_root = self.filesystem_model.index(self.filesystem_model.rootPath())
        self.init_filesystem_treeview()
        try:
            pickle.dump(self.root_path, open(Path(os.path.expanduser("~") + "/Documents/Compare3D/root_path.ini"),
                                             "wb"))
        except:
            print("Can't save to root_path.ini")

    # set's and save's threshold
    def set_threshold_action_handle(self):
        global threshold
        num, ok = QInputDialog.getInt(self, "Enter threshold", "enter a number")

        if ok:
            threshold = num
            try:
                pickle.dump(threshold, open(Path(os.path.expanduser("~") + "/Documents/Compare3D/threshold.ini"), "wb"))
            except:
                print("Can't save threshold.ini")

    # set's and save's exe to open models
    def set_open_in_program_action_handle(self):
        self.open_in_program, filters = QFileDialog.getOpenFileName(self, 'Select program to open your model')
        if os.path.isfile(str(self.open_in_program)):
            try:
                pickle.dump(self.open_in_program, open(Path(os.path.expanduser("~")
                                                            + "/Documents/Compare3D/open_in_prog.ini"), "wb"))
            except:
                print("Can't save open_in_prog.ini")

    # unset's exe to open models
    def unset_open_in_program_action_handle(self):
        self.open_in_program = "_"
        try:
            pickle.dump(self.open_in_program, open(Path(os.path.expanduser("~")
                                                        + "/Documents/Compare3D/open_in_prog.ini"), "wb"))
        except:
            print("Can't save open_in_prog.ini")

    # set's and save's path to java
    def set_java_path_action_handle(self):
        self.java_path, filters = QFileDialog.getOpenFileName(self, "Select path to java.exe")
        try:
            pickle.dump(self.java_path, open(Path(os.path.expanduser("~")
                                                  + "/Documents/Compare3D/java_path.ini"), "wb"))
        except:
            print("Can't save java_path.ini")

    # open file with windows defaul program or with user selected program
    def open_model_in_default_program_action_handle(self, y):
        # global open_in_program
        models = self.get_path_of_selected_items(False)
        similar_models = self.similar_models_get_data(False, False, True, False)
        if not models and not similar_models:
            self.pop_message_box("No model selected")
        elif models and not similar_models:
            if len(models) == 1:  # One model selected
                if self.open_in_program != "_":  # User programme set
                    try:
                        p = subprocess.Popen(self.open_in_program + " " + str(Path(models[0])))
                    except OSError as ex:
                        self.pop_message_box("Can't open in this application because:\n" + str(ex))
                else:  # Open in windows default app
                    try:
                        os.startfile(str(Path(models[0])))
                    except OSError as ex:
                        self.pop_message_box("Can't open in windows default application because:\n" + str(ex))
            else:  # More files selected
                selected = dialog.show_select_file_dialog(self, models, similar_models)
                if selected:  # One file selected
                    try:  # Try user programme
                        p = subprocess.Popen(self.open_in_program + " " + str(Path(selected[0])))
                    except OSError:  # Start in windows deafault
                        try:
                            os.startfile(str(Path(selected[0])))
                        except OSError as ex:
                            self.pop_message_box("Can't open in windows default application because:\n" + str(ex))
        elif not models and similar_models:
            if len(similar_models) == 1:
                if self.open_in_program != "_":  # User programme set
                    try:
                        p = subprocess.Popen(self.open_in_program + " " + str(Path(similar_models[0])))
                    except OSError as ex:
                        self.pop_message_box("Can't open in this application because:\n" + str(ex))
                else:  # Open in windows default app
                    try:
                        os.startfile(str(Path(similar_models[0])))
                    except OSError as ex:
                        self.pop_message_box("Can't open in windows default application because:\n" + str(ex))
            else:  # More files selected
                selected = dialog.show_select_file_dialog(self, models, similar_models)
                if selected:  # One file selected
                    try:  # Try user programme
                        p = subprocess.Popen(self.open_in_program + " " + str(Path(selected[0])))
                    except OSError:  # Start in windows deafault
                        try:
                            os.startfile(str(Path(selected[0])))
                        except OSError as ex:
                            self.pop_message_box("Can't open in windows default application because:\n" + str(ex))
        else:
            # Open dialog to select one file to open
            selected = dialog.show_select_file_dialog(self, models, similar_models)
            if selected:
                try:
                    p = subprocess.Popen(self.open_in_program + " " + str(Path(selected[0])))
                except OSError:
                    try:
                        os.startfile(str(Path(selected[0])))
                    except OSError as ex:
                        self.pop_message_box("Can't open in windows default application because:\n" + str(ex))

    # open windows explorer on selected file destination
    def open_file_destination_action_handle(self, y):
        files = self.get_path_of_selected_items(False)
        files1 = self.similar_models_get_data(False, False, True, False)
        if files is None and files1 is None:
            self.pop_message_box("No file selected")
        elif files and not files1:
            if len(files1) == 1:
                os.startfile(os.path.dirname(str(Path(files[0]))))
            else:
                selected = dialog.show_select_file_dialog(self, files, files1)
                if selected:
                    os.startfile(os.path.dirname(str(Path(selected[0]))))
        elif not files and files1:
            if len(files1) == 1:
                os.startfile(os.path.dirname(str(Path(files1[0]))))
            else:
                selected = dialog.show_select_file_dialog(self, files, files1)
                if selected:
                    os.startfile(os.path.dirname(str(Path(selected[0]))))
        else:
            selected = dialog.show_select_file_dialog(self, files, files1)
            if selected:
                os.startfile(os.path.dirname(str(Path(selected[0]))))

    # set rootpath for view of filesystem
    @staticmethod
    def load_project_structure(p, tree, model):
        model.setRootPath(p)
        tree.setModel(model)
        tree.setRootIndex(model.index(p))

    # set starting position for main window
    def set_main_window_start_position(self, left, top, width, height):
        self.setGeometry(left, top, width, height)
        self.show()

    # Delete selected files in filesystem
    def tree_view_context_menu_delete(self):
        selected_files = self.get_path_of_selected_items(False)
        if selected_files:
            for i in range(len(selected_files)):
                if os.path.isfile(selected_files[i]):
                    try:
                        os.remove(selected_files[i])
                    except OSError as ex:
                        print("Error: " + str(ex))
                if os.path.isdir(selected_files[i]):
                    try:
                        shutil.rmtree(selected_files[i])
                    except OSError as ex:
                        print("Error: " + str(ex))

    # Copy files in filesystem
    def tree_view_context_menu_copy(self):
        self.selected_files_to_copy = self.get_path_of_selected_items(False)

    # Paste files in filesystems
    def tree_view_context_menu_paste(self):
        try:
            selected_files = self.selected_files_to_copy
            if selected_files:
                for i in range(len(selected_files)):
                    if os.path.isfile(selected_files[i]):
                        try:
                            shutil.copy(selected_files[i], self.root_path)
                        except OSError as ex:
                            print("Error: " + str(ex))
                    if os.path.isdir(selected_files[i]):
                        try:
                            head, tail = os.path.split(selected_files[i])
                            print(head + "   " + tail)
                            shutil.copytree(selected_files[i], str(Path(self.root_path + "\\" + tail)))
                        except OSError as ex:
                            print("Error: " + str(ex))
        except:
            print("Nothing to copy")

    # Create empty folder in filesystem
    def tree_view_context_menu_mkdir(self):
        index = self.filesystem_model.index(self.root_path)
        self.tree_view.setCurrentIndex(index)
        d = str(self.filesystem_model.filePath(self.tree_view.currentIndex())) + '/New folder'
        if not os.path.exists(d):
            os.mkdir(d)
        ix = self.filesystem_model.index(d)
        QTimer.singleShot(0, lambda ix=ix: self.tree_view.setCurrentIndex(ix))
        QTimer.singleShot(0, lambda ix=ix: self.tree_view.edit(ix))

    # open similar model via context menu of similar model view
    def similar_model_context_menu_open_in_prog(self, z):
        index = self.similar_model_view.indexAt(z)
        selected_rows = self.similar_models_get_data(False, False, True, False)
        if len(selected_rows) == 1:
            try:
                p = subprocess.Popen(self.open_in_program + " " + str(Path(selected_rows[0])))
            except OSError as ex:
                if self.open_in_program != "_":
                    self.pop_message_box("Can't open in selected application. Try windows default")
                try:
                    os.startfile(str(Path(selected_rows[0])))
                except OSError as ex:
                    self.pop_message_box("Can't open in windows default application because:\n" + str(ex))
        elif index.isValid():
            data = self.similar_models_model.data(index)
            filepath = os.path.realpath(data)
            if os.path.exists(filepath):
                try:
                    p = subprocess.Popen(self.open_in_program + " " + str(Path(filepath)))
                except OSError as ex:
                    if self.open_in_program != "_":
                        self.pop_message_box("Can't open in selected application. Try windows default")
                    try:
                        os.startfile(str(Path(filepath)))
                    except OSError as ex:
                        self.pop_message_box("Can't open in windows default application because:\n" + str(ex))

        else:
            try:
                os.startfile(str(Path(selected_rows[0])))
            except OSError as ex:
                self.pop_message_box("Can't open in windows default application because:\n" + str(ex))

    # open windows explorer with file selected via similar model view context menu
    def similar_model_context_menu_open_in_folder(self, z):
        index = self.similar_model_view.indexAt(z)
        selected_rows = self.similar_models_get_data(False, False, True, False)
        if len(selected_rows) == 1:
            os.startfile(os.path.dirname(str(Path(selected_rows[0]))))
        elif index.isValid():
            data = self.similar_models_model.data(index)
            filepath = os.path.realpath(data)
            if os.path.exists(filepath):
                os.startfile(os.path.dirname(str(Path(selected_rows[0]))))
        else:
            os.startfile(os.path.dirname(str(Path(selected_rows[0]))))

    # show message box with custom message
    def pop_message_box(self, text):
        dial = QMessageBox()
        dial.setText(text)
        dial.setWindowTitle(" ")
        dial.setIcon(QMessageBox.Warning)
        dial.setWindowIcon(QIcon(str(Path(icon_path + "main_window_icon.png"))))
        dial.setGeometry(QtWidgets.QStyle.alignedRect(QtCore.Qt.LeftToRight, QtCore.Qt.AlignCenter, dial.size(),
                                                      QtWidgets.qApp.desktop().availableGeometry()))
        dial.exec_()

    def print_message(self, message):
        print("print_message: " + str(message))


if __name__ == '__main__':
    # get info about  operating system
    system = platform.system()

    # make folder in user's documents
    if not os.path.isdir(Path(os.path.expanduser("~") + "/Documents/Compare3D")):
        os.mkdir(Path(os.path.expanduser("~") + "/Documents/Compare3D"))

    # load path of last viewed model if some exist
    try:
        last_model = pickle.load(open(Path(os.path.expanduser("~") + "/Documents/Compare3D/last_model.ini"), "rb"))
    except(OSError, IOError) as e:
        print("Can't load last_model.ini")

    app = QApplication(sys.argv)
    window = MainWindow()

    # load or select dir with models
    try:
        models_directory_path = pickle.load(open(Path(os.path.expanduser("~") + "/Documents/Compare3D/models_path.ini")
                                                 , "rb"))
    except(OSError, IOError) as e:
        models_directory_path = window.get_models_directory()
        pickle.dump(models_directory_path, open(Path(os.path.expanduser("~") + "/Documents/Compare3D/models_path.ini"),
                                                "wb"))

    # load or select dir with descriptors
    try:
        descriptors_directory_path = pickle.load(open(Path(os.path.expanduser("~") +
                                                           "/Documents/Compare3D/descriptors_path.ini"), "rb"))
    except(OSError, IOError) as e:
        descriptors_directory_path = window.get_descriptors_directory()
        pickle.dump(descriptors_directory_path, open(Path(os.path.expanduser("~")
                                                          + "/Documents/Compare3D/descriptors_path.ini"), "wb"))
    # load user selected threshold or save default threshold
    try:
        threshold = pickle.load(open(Path(os.path.expanduser("~") + "/Documents/Compare3D/threshold.ini"), "rb"))
    except:
        pickle.dump(threshold, open(Path(os.path.expanduser("~") + "/Documents/Compare3D/threshold.ini"), "wb"))

    # load 3rd  party program path
    try:
        open_in_program = pickle.load(open(Path(os.path.expanduser("~")
                                                + "/Documents/Compare3D/open_in_prog.ini"), "rb"))
    except:
        pickle.dump(open_in_program, open(Path(os.path.expanduser("~")
                                               + "/Documents/Compare3D/open_in_prog.ini"), "wb"))
    # load or save path to java
    try:
        window.java_path = pickle.load(open(Path(os.path.expanduser("~")
                                                 + "/Documents/Compare3D/java_path.ini"), "rb"))
        if not window.java_path:
            window.java_path, filters = QFileDialog.getOpenFileName(window, "Select path to java.exe")
            pickle.dump(window.java_path, open(Path(os.path.expanduser("~")
                                                    + "/Documents/Compare3D/java_path.ini"), "wb"))
    except:
        window.java_path, filters = QFileDialog.getOpenFileName(window, "Select path to java.exe")
        pickle.dump(window.java_path, open(Path(os.path.expanduser("~")
                                                + "/Documents/Compare3D/java_path.ini"), "wb"))

    app.exec_()
    # save aplication variables
    try:
        pickle.dump(last_model, open(Path(os.path.expanduser("~") + "/Documents/Compare3D/last_model.ini"), "wb"))
    except:
        print("Can't write to last_model.ini :(")

    try:
        pickle.dump(models_directory_path, open(Path(os.path.expanduser("~")
                                                     + "/Documents/Compare3D/models_path.ini"), "wb"))
    except:
        print("Can't write to models_path.ini")

    try:
        pickle.dump(descriptors_directory_path, open(Path(os.path.expanduser("~")
                                                          + "/Documents/Compare3D/descriptors_path.ini"), "wb"))
    except:
        print("Can't write to descriptors_path.ini")

    try:
        pickle.dump(window.java_path, open(Path(os.path.expanduser("~")
                                                + "/Documents/Compare3D/java_path.ini"), "wb"))
    except:
        print("Can't write to java_path.ini")
