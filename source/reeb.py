import ntpath
import os
from os import listdir
from os.path import isfile, join
import pickle
import subprocess

import psutil
from PyQt5.QtCore import QThread, pyqtSignal

import loading
from PyQt5.QtWidgets import QSizePolicy, QDialog
from pathlib import *

win_javac_path = str(Path('D:/InstallPrograms/openjdk-14.0.1_windows-x64_bin/jdk-14.0.1/bin/javac.exe'))


def compile_reeb_graph():
    compiled = True
    for file in os.listdir("reeb_graph"):
        if file.endswith(".java"):
            a = os.path.splitext(file)[0]
            a = a + ".class"
            if not os.path.isfile("reeb_graph\\" + a):
                compiled = False
                break

    if not compiled:
        subprocess.Popen(win_javac_path + " reeb_graph/*.java -Xlint")


def extract_descriptors(self, models_dir, descriptors_dir):
    self.up_button.setEnabled(False)
    widgets_index = self.layout_2.count()
    if widgets_index == 3:
        myWidget = self.layout_2.itemAt(widgets_index - 1).widget()
        myWidget.setParent(None)
    if self.java_path:
        self.progressbar = loading.Window(self, models_dir, descriptors_dir, self.java_path)
        self.progressbar.setMinimumWidth(10)
        self.progressbar.setMaximumHeight(20)
        self.progressbar.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        self.layout_2.addWidget(self.progressbar)
        self.up_button.setEnabled(True)
    else:
        self.pop_message_box("Please set path to java.exe")


# compare models descriptors
def compare_models(self, models_dir, model_to_compare, threshold, desc_path):
    try:
        # remove old data in rows
        self.similar_models_model.removeRows(0, self.similar_models_model.rowCount())
    except:
        print("self.similar_models_model.removeRows: failed")
    if self.java_path:

        # add group box with results to main window
        self.top_layout.addWidget(self.similar_model_group_box, 0, 2)

        strart_comparing = ComapareModels(self, models_dir, model_to_compare, threshold, desc_path, self.java_path)
        strart_comparing.start()
    else:
        self.pop_message_box("Please set path to java.exe")


# Start model comparing
class ComapareModels:
    def __init__(self, main_window, models_dir, model_to_compare, threshold, desc_path, java_path):
        super().__init__()
        self.main_window = main_window
        self.models_dir = models_dir
        self.model_to_compare = model_to_compare
        self.threshold = threshold
        self.desc_path = desc_path
        self.java_path = java_path
        self.thread = ComapareModelsThread(self.main_window, self.models_dir, self.model_to_compare, self.threshold,
                                           self.desc_path, self.java_path)

    def start(self):
        self.thread.run()


# Thread that compares models on backround
class ComapareModelsThread(QThread):
    def __init__(self, main_window, models_dir, model_to_compare, threshold, desc_path, java_path):
        super().__init__()
        self.main_window = main_window
        self.models_dir = str(Path(models_dir))
        self.model_to_compare = str(Path(model_to_compare))
        self.threshold = threshold
        self.desc_path = str(Path(desc_path))
        self.java_path = str(Path(java_path))

    def run(self):
        # find similar models
        similar_models = []

        p = subprocess.Popen(self.java_path + " -cp \"reeb_graph/\" -Xmx1024m "
                                              "CompareReebGraph 4000 0.0005 128 0.5 " + str(Path(self.models_dir))
                             + " " + str(Path(self.model_to_compare)) + " " + str(Path(self.desc_path)),
                             stderr=subprocess.PIPE, stdout=subprocess.PIPE)

        # cath output of comparing subprocess
        i = 0
        while True:
            line = p.stdout.readline()
            # line2 = p.stderr.readline()
            # if line2:
            #     print(line2)

            if not line:
                break
            line = line.decode("utf-8")
            print(line)
            split = line.split("|")
            # check if line has all we need to save information
            if len(split) == 5:
                if float(split[3]) >= float(self.threshold):
                    i = i + 1
                    # save path to model and similarity with model_to_compare
                    similar_models.append(split[1] + "|" + split[3])

        for i in range(len(similar_models)):
            tmp = similar_models[i].split("|")
            path = str(Path(tmp[0]))
            similarity = float(tmp[1]).__round__(3)
            name = os.path.basename(Path(tmp[0]))
            self.main_window.similar_model_add_row(name, path, similarity)

        # resize main window
        if self.main_window.frameGeometry().width() <= 1060:
            self.main_window.set_main_window_start_position(self.main_window.pos().x() + 0,
                                                            self.main_window.pos().y() + 30,
                                                            self.main_window.frameGeometry().width() + 260,
                                                            self.main_window.frameGeometry().height())
        print("Finished")
        return 0


# thread that runs descriptors extraction on backround
class MyThread(QThread):
    """
        Thread that runs descriptors extraction on backround

        ...

        Methods
        -------
        run
            extract descriptors
        setProgressVal
            update number of extracted models
        stop
            stop extraction

        """

    def __init__(self, main_window, models_dir, descriptors_dir, java_path, wrl_files, models_to_extract, old_files):
        super().__init__()
        self.models_dir = str(Path(models_dir))
        self.descriptors_dir = str(Path(descriptors_dir))
        self.java_path = str(Path(java_path))
        self.wrl_files = wrl_files
        self.models_to_extract = models_to_extract
        self.old_files = old_files
        self.mrg_files = []
        self.main_window = main_window
        self.threadactive = True

    # Create a counter thread
    change_value = pyqtSignal(int)

    # run thread
    def run(self):
        # if there is something to change, do it
        if self.models_to_extract:
            printArray(self.models_to_extract, "models_to_extract")

            models_to_extract_string = ""
            for i in range(len(self.models_to_extract)):
                models_to_extract_string = models_to_extract_string + str(Path(self.models_to_extract[i])) + "|"
            print("Extraction started")
            p = subprocess.Popen(self.java_path + ' -cp \"reeb_graph/\" -Xmx1024m ExtractReebGraph 4000 0.0005 128 '
                                 + self.models_dir + ' ' + self.descriptors_dir + ' '
                                 + '\"' + models_to_extract_string + '\"',
                                 shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)

            # Change .wrl to .mrg to find out which files were extracted
            for x in range(len(self.wrl_files)):
                tmp = os.path.splitext(self.wrl_files[x])[0]
                tmp = os.path.basename(tmp)
                self.mrg_files.append(tmp + ".mrg\r\n")

            file_count = len(self.mrg_files)
            count = 0
            # Watch for filenames to count how many files were converted

            while True:
                if not self.threadactive:
                    try:
                        p.wait(timeout=1)
                    except subprocess.TimeoutExpired:
                        process = psutil.Process(p.pid)
                        for proc in process.children(recursive=True):
                            proc.kill()
                        process.kill()
                        self.main_window.extract_descriptors_button.setEnabled(True)
                        widgets_index = self.main_window.layout_2.count()
                        if widgets_index == 3:
                            myWidget = self.main_window.layout_2.itemAt(widgets_index - 1).widget()
                            myWidget.setParent(None)
                        self.main_window.layout_2.addWidget(self.main_window.dummy_progressbar)
                        break

                line = p.stdout.readline()
                if not line:
                    break
                line = line.decode("utf-8")
                print(line)
                # emit number of files extracted to progress bar
                if any(line in s for s in self.mrg_files):
                    count += 1
                    self.change_value.emit(count)
                if count == file_count:
                    break

            # edit info about modified files and append new ones
            for i in range(len(self.old_files)):
                split = self.old_files[i].split("|")
                if any(split[0] in s for s in self.models_to_extract):
                    for j in range(len(self.models_to_extract)):
                        if split[0] == self.models_to_extract[j]:
                            size = Path(self.models_to_extract[j]).stat().st_size
                            self.old_files[i] = self.models_to_extract[j] + "|" + str(size)

            for i in range(len(self.models_to_extract)):
                if any(self.models_to_extract[i] in s for s in self.old_files):
                    continue
                else:
                    self.old_files.append(self.models_to_extract[i] + "|"
                                          + str(Path(self.models_to_extract[i]).stat().st_size))

            # Get list of descriptors
            descriptors = [f for f in listdir(self.descriptors_dir) if isfile(join(self.descriptors_dir, f))]

            list_of_models = getListOfFiles(self.models_dir)

            if str(list_of_models) == "-1":
                self.main_window.pop_message_box("Please select models directory")
                return

            # Get list of models with .mrg extension
            for i in range(len(list_of_models)):
                base_name = ntpath.basename(list_of_models[i])
                filename, extension = os.path.splitext(base_name)
                if extension == ".wrl":
                    extension = ".mrg"
                    list_of_models[i] = filename + extension

            # delete descriptors of files that were deleted
            for i in range(len(descriptors)):
                if any(descriptors[i] in s for s in list_of_models):
                    continue
                else:
                    filename, extension = os.path.splitext(descriptors[i])
                    if extension == ".mrg":
                        print("to delete: " + self.descriptors_dir + "\\" + descriptors[i])
                        os.remove(self.descriptors_dir + "\\" + descriptors[i])
            # save list of files and filesizes
            try:
                pickle.dump(self.old_files,
                            open(Path(os.path.expanduser("~") + "/Documents/Compare3D/wrl_files_info.ini"), "wb"))
            except:
                print("Can't write to wrl_files_info.ini :(")
        # enable extract button in app
        self.main_window.extract_descriptors_button.setEnabled(True)

    def stop(self):
        self.threadactive = False


# returns list of files in directory and it's subdirectories
def getListOfFiles(dirName):
    # create a list of file and sub directories
    # names in the given directory
    try:
        listOfFile = os.listdir(dirName)
    except OSError as ex:
        return "-1"
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(str(Path(fullPath)))

    return allFiles


def printArray(array, name):
    if array:
        for i in range(len(array)):
            print(str(name) + "[" + str(i) + "]: " + str(array[i]))

        print(str(name) + "length: " + str(len(array)))
