from PyQt5.Qt3DCore import QTransform
from PyQt5.Qt3DExtras import QOrbitCameraController
from PyQt5.QtCore import *
from PyQt5.QtGui import QVector3D, QMatrix4x4, QQuaternion

axis_x = 1.0
axis_y = 0.0
axis_z = 0.0
rotation_angle = 25.0


# create view of model for container in main window
def view_model(self, model):
    try:
        self.scene = createScene(self, model)
        self.camera.lens().setPerspectiveProjection(45.0, 16.0 / 9.0, 0.1, 1000.0)
        self.camera.setPosition(QVector3D(0.0, 0.0, 40.0))
        self.camera.setViewCenter(QVector3D(0.0, 0.0, 0.0))
        # For camera controls.
        camController = QOrbitCameraController(self.scene)
        # camController = QFirstPersonCameraController(self.scene)
        camController.setLinearSpeed(50.0)
        camController.setLookSpeed(180.0)
        camController.setCamera(self.camera)

        self.view.setRootEntity(self.scene)

        self.view.show()
    except(OSError, IOError) as e:
        print("Cant display this :(")


# create scene for view
def createScene(self, create_scene_model_path):
    global axis_x
    global axis_y
    global axis_z
    global rotation_angle
    self.torusMesh.setSource(QUrl.fromLocalFile(create_scene_model_path))
    self.torusTransform.setScale3D(QVector3D(1.5, 1.0, 1.0))
    self.torusTransform.setRotation(QQuaternion.fromAxisAndAngle(QVector3D(axis_x, axis_y, axis_z), rotation_angle))
    self.torusEntity.addComponent(self.torusMesh)
    self.torusEntity.addComponent(self.torusTransform)
    self.torusEntity.addComponent(self.material)
    return self.rootEntity


def rotate_model_down_action_handle(self, y):
    global axis_x
    axis_x = axis_x + 25
    self.torusTransform.setRotationX(axis_x)


def rotate_model_up_action_handle(self, y):
    global axis_x
    axis_x = axis_x - 25
    self.torusTransform.setRotationX(axis_x)


def rotate_model_left_action_handle(self, y):
    global axis_y
    axis_y = axis_y + 25
    self.torusTransform.setRotationY(axis_y)


def rotate_model_right_action_handle(self, y):
    global axis_y
    axis_y = axis_y - 25
    self.torusTransform.setRotationY(axis_y)
